package ru.hardwork.onlinesocialdiagnosticapp.common;

import java.util.ArrayList;
import java.util.List;

import ru.hardwork.onlinesocialdiagnosticapp.Model.Category;
import ru.hardwork.onlinesocialdiagnosticapp.Model.DiagnosticTest;
import ru.hardwork.onlinesocialdiagnosticapp.Model.Question;
import ru.hardwork.onlinesocialdiagnosticapp.Model.User;

public class Common {
    public static String diagnosticId;
    public static User currentUser;
    public static List<Category> categoryList = new ArrayList<>();
    public static List<DiagnosticTest> diagnosticTests = new ArrayList<>();
    public static List<Question> questions = new ArrayList<>();

}
