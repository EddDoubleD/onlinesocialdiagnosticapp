package ru.hardwork.onlinesocialdiagnosticapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import org.apache.commons.lang3.StringUtils;

import ru.hardwork.onlinesocialdiagnosticapp.Model.Question;
import ru.hardwork.onlinesocialdiagnosticapp.common.Common;

import static java.lang.String.format;

public class Diagnostic extends AppCompatActivity implements View.OnClickListener {

    final static long INTERVAL = 1000;
    final static long TIMEOUT = 7000;
    int progressValue = 0;

    int index = 0, score = 0, thisQuestion = 0, totalQuestion, correctAnswer;

    ProgressBar progressBar;
    ImageView questionImage;
    Button btnI, btnII, btnIII, btnIV;
    TextView txtScore, txtQuestionNum, questionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_diagnostic);
        // Views
        txtScore = findViewById(R.id.txtScore);
        txtQuestionNum = findViewById(R.id.txtTotalQuestion);
        questionText = findViewById(R.id.question_text);
        questionImage = findViewById(R.id.question_image);

        progressBar = findViewById(R.id.progressBar);

        btnI = findViewById(R.id.btnAnswerI);
        btnII = findViewById(R.id.btnAnswerII);
        btnIII = findViewById(R.id.btnAnswerIII);
        btnIV = findViewById(R.id.btnAnswerIV);

        btnI.setOnClickListener(this);
        btnII.setOnClickListener(this);
        btnIII.setOnClickListener(this);
        btnIV.setOnClickListener(this);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onClick(View view) {
        if (index < totalQuestion) {
            Button clicked = (Button) view;
            Question question = Common.questions.get(index);
            String costs = question.getCosts();
            String[] arr = StringUtils.split(costs, ",");
            int pos = question.findClickedAnswerPosition(clicked.getText().toString());
            score += pos >= 0 ? Integer.parseInt(arr[pos]) : 0;

            showQuestion(++index);
        } else {
            Intent done = new Intent(this, Done.class);
            Bundle dataSend = new Bundle();
            dataSend.putInt("SCORE", score);
            dataSend.putInt("TOTAL", totalQuestion);
            done.putExtras(dataSend);
            startActivity(done);
            finish();
        }

        txtScore.setText(format("%d", score));
    }

    @SuppressLint("DefaultLocale")
    private void showQuestion(int index) {
        if (index < totalQuestion) {
            txtQuestionNum.setText(format("%d / %d", thisQuestion, totalQuestion));
            progressBar.setProgress(0);
            progressValue = 0;

            Question question = Common.questions.get(index);
            if (question.isImageQuestion()) {
                // todo upload photo, not used yet
                Picasso.get()
                        .load(question.getText())
                        .centerCrop()
                        .into(questionImage);
                questionImage.setVisibility(View.VISIBLE);
                questionText.setVisibility(View.INVISIBLE);
            } else {
                questionImage.setVisibility(View.INVISIBLE);
                questionText.setVisibility(View.VISIBLE);
                questionText.setText(question.getText());
            }

            btnI.setText(question.getAnsversI());
            btnII.setText(question.getAnsversII());
            btnIII.setText(question.getAnsversIII());
            btnIV.setText(question.getAnsversIV());
        } else {
            Intent done = new Intent(this, Done.class);
            Bundle dataSend = new Bundle();
            dataSend.putInt("SCORE", score);
            dataSend.putInt("TOTAL", totalQuestion);
            done.putExtras(dataSend);
            startActivity(done);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        totalQuestion = Common.questions.size();
        showQuestion(index);
    }
}