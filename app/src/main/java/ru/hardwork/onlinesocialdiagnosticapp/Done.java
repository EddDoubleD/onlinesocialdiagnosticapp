package ru.hardwork.onlinesocialdiagnosticapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import ru.hardwork.onlinesocialdiagnosticapp.Model.QuestionResult;
import ru.hardwork.onlinesocialdiagnosticapp.common.Common;

import static java.lang.String.format;

public class Done extends AppCompatActivity {

    Button btnTryAgain;
    TextView textTotalScore, txtMessage;

    ProgressBar progressBar;

    FirebaseDatabase database;
    DatabaseReference question_result;


    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done);

        database = FirebaseDatabase.getInstance();
        question_result = database.getReference("Question_Result");

        textTotalScore = findViewById(R.id.textTotalScore);
        txtMessage = findViewById(R.id.txtMessage);

        progressBar = findViewById(R.id.doneProgressBar);
        btnTryAgain = findViewById(R.id.btnTryAgain);

        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Done.this, Home.class);
                startActivity(intent);
                finish();
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            int score = extras.getInt("SCORE");
            int totalQuestions = extras.getInt("TOTAL");

            textTotalScore.setText(format("SCORE: %d", score));
            txtMessage.setText("Все хорошо!");

            progressBar.setMax(totalQuestions);
            progressBar.setProgress(2);
            String query = format("%s_%s", Common.currentUser.getLogIn(), Common.diagnosticId);
            question_result.child(query)
                    .setValue(new QuestionResult(query, Common.diagnosticId, Common.currentUser.getLogIn(), String.valueOf(score)));
        }
    }

}