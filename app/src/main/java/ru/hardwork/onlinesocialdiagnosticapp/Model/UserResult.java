package ru.hardwork.onlinesocialdiagnosticapp.Model;

public class UserResult {
    private String user;
    private long score;

    public UserResult(String user, long score) {
        this.user = user;
        this.score = score;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }
}
