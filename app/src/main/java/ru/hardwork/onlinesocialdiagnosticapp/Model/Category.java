package ru.hardwork.onlinesocialdiagnosticapp.Model;

public class Category {

    private String Name; // category name
    private int count;

    public Category() {

    }

    public Category(String name, int count) {
        this.Name = name;
        this.count = count;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
