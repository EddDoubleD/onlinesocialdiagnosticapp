package ru.hardwork.onlinesocialdiagnosticapp.Model;

public class QuestionResult {
    private String questionResult;
    private String categoryId;
    private String user;
    private String score;

    public QuestionResult() {

    }

    public QuestionResult(String questionResult, String categoryId, String user, String score) {

        this.questionResult = questionResult;
        this.categoryId = categoryId;
        this.user = user;
        this.score = score;
    }

    public String getQuestionResult() {
        return questionResult;
    }

    public void setQuestionResult(String questionResult) {
        this.questionResult = questionResult;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
