package ru.hardwork.onlinesocialdiagnosticapp.Model;

import org.apache.commons.lang3.StringUtils;

public class Question {
    private String text;
    private String ansversI;
    private String ansversII;
    private String ansversIII;
    private String ansversIV;
    private String costs;
    private boolean isImageQuestion;

    public Question() {

    }

    public Question(String text, String ansversI, String ansversII, String ansversIII, String ansversIV, String costs, boolean isImageQuestion) {
        this.text = text;
        this.ansversI = ansversI;
        this.ansversII = ansversII;
        this.ansversIII = ansversIII;
        this.ansversIV = ansversIV;
        this.costs = costs;
        this.isImageQuestion = isImageQuestion;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnsversI() {
        return ansversI;
    }

    public void setAnsversI(String ansversI) {
        this.ansversI = ansversI;
    }

    public String getAnsversII() {
        return ansversII;
    }

    public void setAnsversII(String ansversII) {
        this.ansversII = ansversII;
    }

    public String getAnsversIII() {
        return ansversIII;
    }

    public void setAnsversIII(String ansversIII) {
        this.ansversIII = ansversIII;
    }

    public String getAnsversIV() {
        return ansversIV;
    }

    public void setAnsversIV(String ansversIV) {
        this.ansversIV = ansversIV;
    }

    public String getCosts() {
        return costs;
    }

    public void setCosts(String costs) {
        this.costs = costs;
    }

    public boolean isImageQuestion() {
        return isImageQuestion;
    }

    public void setImageQuestion(boolean imageQuestion) {
        isImageQuestion = imageQuestion;
    }

    public int findClickedAnswerPosition(String question) {
        if (question.equals(StringUtils.trim(ansversI))) {
            return 0;
        }

        if (question.equals(StringUtils.trim(ansversII))) {
            return 1;
        }

        if (question.equals(StringUtils.trim(ansversIII))) {
            return 2;
        }

        if (question.equals(StringUtils.trim(ansversIV))) {
            return 3;
        }
        return -1;
    }
}
