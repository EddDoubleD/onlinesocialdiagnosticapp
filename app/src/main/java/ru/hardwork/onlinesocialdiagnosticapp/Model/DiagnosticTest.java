package ru.hardwork.onlinesocialdiagnosticapp.Model;

public class DiagnosticTest {
    private String name;
    private String description;
    private String categoryId;
    private String questionCount;
    private String testDuration;
    private String metricId;

    public DiagnosticTest() {

    }

    public DiagnosticTest(String name, String description, String categoryId, String questionCount, String testDuration, String metricId) {
        this.name = name;
        this.description = description;
        this.categoryId = categoryId;
        this.questionCount = questionCount;
        this.testDuration = testDuration;
        this.metricId = metricId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getQuestionCount() {
        return questionCount;
    }

    public void setQuestionCount(String questionCount) {
        this.questionCount = questionCount;
    }

    public String getTestDuration() {
        return testDuration;
    }

    public void setTestDuration(String testDuration) {
        this.testDuration = testDuration;
    }

    public String getMetricId() {
        return metricId;
    }

    public void setMetricId(String metricId) {
        this.metricId = metricId;
    }
}
