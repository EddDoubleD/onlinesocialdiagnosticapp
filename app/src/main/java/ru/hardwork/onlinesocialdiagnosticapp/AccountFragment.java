package ru.hardwork.onlinesocialdiagnosticapp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ru.hardwork.onlinesocialdiagnosticapp.Model.QuestionResult;
import ru.hardwork.onlinesocialdiagnosticapp.Model.UserResult;
import ru.hardwork.onlinesocialdiagnosticapp.callback.UserResultCallback;
import ru.hardwork.onlinesocialdiagnosticapp.common.Common;

/**
 *
 */
public class AccountFragment extends Fragment {

    View mFragment;

    FirebaseDatabase database;
    DatabaseReference questionResult;
    DatabaseReference userResultTbl;

    int sum = 0;

    public static AccountFragment newInstance() {
        AccountFragment accountFragment = new AccountFragment();
        return accountFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = FirebaseDatabase.getInstance();
        questionResult = database.getReference("Question_Result");
        userResultTbl = database.getReference("User_Results");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragment = inflater.inflate(R.layout.fragment_account, container, false);

        updateScore(Common.currentUser.getLogIn(), new UserResultCallback<UserResult>() {
            @Override
            public void call(UserResult userResult) {
                userResultTbl.child(userResult.getUser()).setValue(userResult);
                //showUserResults();
            }
        });

        return mFragment;
    }

    private void showUserResults() {
        userResultTbl.orderByChild("score")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot data : snapshot.getChildren()) {
                            UserResult local = data.getValue(UserResult.class);

                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }

    private void updateScore(final String logIn, final UserResultCallback<UserResult> callback) {
        questionResult.orderByChild("Users").equalTo(logIn)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot data : snapshot.getChildren()) {
                            QuestionResult questionResult = data.getValue(QuestionResult.class);
                            sum += Integer.parseInt(questionResult.getScore());
                        }
                        UserResult userResult = new UserResult(logIn, sum);
                        callback.call(userResult);
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });
    }
}