package ru.hardwork.onlinesocialdiagnosticapp.scenery;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int verticalSpaceHeight;
    private final int horizontalSpaceHeight;

    public VerticalSpaceItemDecoration(int verticalSpaceHeight, int horizontalSpaceHeight) {
        this.verticalSpaceHeight = verticalSpaceHeight;
        this.horizontalSpaceHeight = horizontalSpaceHeight;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.bottom = verticalSpaceHeight;
        outRect.left = horizontalSpaceHeight / 2;
        outRect.right = horizontalSpaceHeight / 2;
    }
}
