package ru.hardwork.onlinesocialdiagnosticapp.holders;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ru.hardwork.onlinesocialdiagnosticapp.R;
import ru.hardwork.onlinesocialdiagnosticapp.listener.ItemClickListener;

public class DiagnosticTestViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView diagnosticName;
    public TextView description;
    // Слушатель клика
    private ItemClickListener itemClickListener;

    public DiagnosticTestViewHolder(@NonNull View itemView) {
        super(itemView);
        diagnosticName = itemView.findViewById(R.id.diagnosticName);
        description = itemView.findViewById(R.id.description);

        itemView.setOnClickListener(this);
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }


    @Override
    public void onClick(View view) {
        itemClickListener.onClick(view, getAdapterPosition(), false);
    }
}
