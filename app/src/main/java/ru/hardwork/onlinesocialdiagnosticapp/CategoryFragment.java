package ru.hardwork.onlinesocialdiagnosticapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import ru.hardwork.onlinesocialdiagnosticapp.Model.Category;
import ru.hardwork.onlinesocialdiagnosticapp.Model.DiagnosticTest;
import ru.hardwork.onlinesocialdiagnosticapp.common.Common;
import ru.hardwork.onlinesocialdiagnosticapp.holders.DiagnosticTestViewHolder;
import ru.hardwork.onlinesocialdiagnosticapp.listener.ItemClickListener;
import ru.hardwork.onlinesocialdiagnosticapp.scenery.VerticalSpaceItemDecoration;

/**
 *
 */
public class CategoryFragment extends Fragment {
    // views
    View mFragment;
    // Адаптер для загрузки категорий в CategoryViewHolder
    FirebaseRecyclerAdapter<DiagnosticTest, DiagnosticTestViewHolder> adapter;
    FirebaseDatabase database;
    DatabaseReference categories;
    DatabaseReference diagnosticTests;
    ArrayList<String> arrayList = new ArrayList<>();
    // diagnosticRecycler
    private TabLayout tabLayout;
    private RecyclerView mRecyclerView;
    private boolean isUserScrolling = false;
    private boolean isListGoingUp = true;
    private boolean tabSelected = true;

    public static CategoryFragment newInstance() {
        CategoryFragment categoryFragment = new CategoryFragment();
        return categoryFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //
        database = FirebaseDatabase.getInstance();
        categories = database.getReference("Category");
        diagnosticTests = database.getReference("DiagnosticTest");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mFragment = inflater.inflate(R.layout.fragment_category, container, false);


        mRecyclerView = mFragment.findViewById(R.id.diagnosticTestsRecycler);
        //  TabLayout Code end
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(
                this.getContext(),
                LinearLayoutManager.HORIZONTAL,
                false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(10, 70));

        tabLayout = mFragment.findViewById(R.id.categoryTab);
        // Загружаем категории
        loadCategories();

        //   Код для расстояния между вкладками
        int betweenSpace = 20;
        ViewGroup slidingTabStrip = (ViewGroup) tabLayout.getChildAt(0);
        for (int i = 0; i < slidingTabStrip.getChildCount() - 1; i++) {
            View v = slidingTabStrip.getChildAt(i);
            ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) v.getLayoutParams();
            params.rightMargin = betweenSpace;
        }

        //  Code привязки start
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                isUserScrolling = false;
                int position = tab.getPosition();

                int testPosition = 0;
                for (int i = 0; i < position; i++) {
                    testPosition += Common.categoryList.get(i).getCount();
                }
                if (tabSelected) {
                    mLayoutManager.scrollToPositionWithOffset(testPosition, 0);
                    //--- код для прокрутки
                    //psyTestsRecycler.smoothScrollToPosition(testPosition);
                } else {
                    tabSelected = true;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        loadDiagnosticTests();

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    isUserScrolling = true;
                    if (isListGoingUp) {
                        if (mLayoutManager.findLastCompletelyVisibleItemPosition() + 1 == arrayList.size()) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                }
                            }, 50);
                        }
                    }
                }
            }

           /* @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int itemPosition = mLayoutManager.findFirstVisibleItemPosition();

                final DiagnosticTest diagnosticTest = Common.diagnosticTests.get(itemPosition);
                Category category = Iterables.find(Common.categoryList, new Predicate<Category>() {
                    @Override
                    public boolean apply(@Nullable Category cat) {
                        return cat != null;// && diagnosticTest.getCategoryId() == cat.getId();
                    }
                });

                int currentCatPosition = Common.categoryList.indexOf(category);
                // Если скроллит пользователь сместим таб
                if (isUserScrolling) {
                    int tabPosition = tabLayout.getSelectedTabPosition();
                    if (currentCatPosition != tabPosition) {
                        TabLayout.Tab tab = tabLayout.getTabAt(currentCatPosition);
                        tabSelected = false;
                        assert tab != null;
                        tab.select();
                    }
                }
            }*/
        });
        return mFragment;
    }


    private void loadCategories() {
        categories.orderByChild("name")
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot data : snapshot.getChildren()) {
                            Category category = data.getValue(Category.class);
                            Common.categoryList.add(category);
                            tabLayout.addTab(tabLayout.newTab().setText(category.getName()));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {
                        Log.e(error.getMessage(), error.getDetails());
                    }
                });
    }

    /**
     * load "Categories" from Firebase
     */
    private void loadDiagnosticTests() {
        // Options for recycler view by Firebase
        FirebaseRecyclerOptions<DiagnosticTest> options = new FirebaseRecyclerOptions
                .Builder<DiagnosticTest>()
                .setQuery(diagnosticTests, DiagnosticTest.class)
                .build();
        // Create adapter
        adapter = new FirebaseRecyclerAdapter<DiagnosticTest, DiagnosticTestViewHolder>(options) {
            @NonNull
            @Override
            public DiagnosticTestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(getContext());
                View view = inflater.inflate(R.layout.item_psytest_version_2, parent, false);
                return new DiagnosticTestViewHolder(view);
            }

            @Override
            protected void onBindViewHolder(@NonNull DiagnosticTestViewHolder holder, int position, @NonNull final DiagnosticTest model) {
                holder.diagnosticName.setText(model.getName());
                holder.description.setText(model.getDescription());
                Common.diagnosticTests.add(model);
                holder.setItemClickListener(new ItemClickListener() {
                    @SuppressLint("ShowToast")
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        //Toast.makeText(getContext(), String.format("%s | %s", adapter.getRef(position).getKey(), model.getName()), Toast.LENGTH_SHORT).show();
                        Intent startDiagnostic = new Intent(getActivity(), Start.class);
                        Common.diagnosticId = adapter.getRef(position).getKey();
                        startActivity(startDiagnostic);
                    }
                });
            }

        };
        adapter.notifyDataSetChanged();
        adapter.startListening();
        mRecyclerView.setAdapter(adapter);
    }
}