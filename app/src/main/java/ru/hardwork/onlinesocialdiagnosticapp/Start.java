package ru.hardwork.onlinesocialdiagnosticapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import ru.hardwork.onlinesocialdiagnosticapp.Model.Question;
import ru.hardwork.onlinesocialdiagnosticapp.common.Common;

public class Start extends AppCompatActivity {

    FirebaseDatabase database;
    DatabaseReference questions;

    Button btnPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        database = FirebaseDatabase.getInstance();
        questions = database.getReference("Questions");

        loadQuestions(Common.diagnosticId);

        btnPlay = findViewById(R.id.btnPlay);
        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Start.this, Diagnostic.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void loadQuestions(String categoryId) {
        if (Common.questions.size() > 0) {
            Common.questions.clear();
        }

        questions.orderByChild("categoryId").equalTo(categoryId)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                            Question question = postSnapshot.getValue(Question.class);
                            Common.questions.add(question);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError error) {

                    }
                });

    }
}