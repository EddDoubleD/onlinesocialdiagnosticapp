package ru.hardwork.onlinesocialdiagnosticapp.common;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommonTest {

    @Test
    public void simpleTest() {
        Common common = new Common();
        Assert.assertNotNull(common.categoryList);
        Assert.assertNotNull(common.diagnosticTests);
        Assert.assertNotNull(common.questions);
    }

}